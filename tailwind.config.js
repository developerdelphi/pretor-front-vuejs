module.exports = {
  theme: {
    fontFamily: {
      sans: 'Montserrat'
    },
    extend: {
      colors: {
        primary: '#2C3E50',
        secondary: '#95a5a6',
        terciary: '#0f7864',
        sindigo: '#6610f2',
        spurple: '#6f42c1',
        spink: '#e83e8c',
        sred: '#E74C3C',
        sorange: '#fd7e14',
        syellow: '#F39C12',
        sgreen: '#18BC9C',
        steal: '#20c997',
        scyan: '#3498DB'
      }
    }
  },
  variants: {},
  plugins: []
}
