import Errors from './errorsClass'
// import BaseAxios from '@/services/http/conf'
export default class FormClass {
  constructor (data) {
    this.form = data
    for (const field in data) {
      this[field] = data[field]
    }
    this.errors = new Errors()
  }

  data () {
    var data = {}

    for (const field in this.form) {
      data[field] = this[field]
    }

    // const data = Object.assign({}, this)

    // delete data.form
    // delete data.errors

    return data
  }

  submit (url) {
    // console.log(vm)
    // console.log('filds do post: ', this.data())
    // BaseAxios.post(url, this.data())
    //   // .then(this.onSuccess(.bind(this))
    //   .then(response => {
    //     const { data } = response
    //     const resp = JSON.parse(data)
    //     const { message } = resp.data
    //     // console.log('sucesso', typeof message)
    //     // vm.$toast.success(message, 'Sucesso', vm.$root._data.success)
    //     // this.$toast.success(`${message}`, 'Sucesso', this.$root._data.success)
    //     this.reset()
    //   })
    //   // .catch(this.onFail().bind(this))
    //   .catch(error => {
    //     // vm.$toast.error(
    //     //   'Atenção! Verifique os dados do formulário!',
    //     //   'Falha',
    //     //   vm.$root._data.error
    //     // )
    //     const { errors } = JSON.parse(error.response.data)
    //     this.errors.record(errors)
    //   })
  }

  // onSuccess (response) {
  //   const vm = this
  //   const { data } = response
  //   const resp = JSON.parse(data)
  //   const { message } = resp.data
  //   console.log('sucesso', typeof message)
  //   vm.$toast.success('tentando', 'Sucesso', vm.$root._data.success)
  //   // this.$toast.success(`${message}`, 'Sucesso', this.$root._data.success)
  //   this.reset()
  // }

  // onFail (fail) {
  //   // console.log('falha: ', fail.response)
  //   const { errors } = JSON.parse(fail.response.data)
  //   this.errors.record(errors)
  // }
  setErrors ({ errors }) {
    console.log('mesagem form class setErros: ', errors)
    this.clearErrors()
    this.errors.record(errors)
  }

  clearErrors () {
    this.errors.clear()
  }

  reset () {
    for (const field in this.form) {
      this[field] = ''
    }
  }
}
