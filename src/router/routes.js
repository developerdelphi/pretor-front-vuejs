import { routes as areas } from '@/modules/areas'
import { routes as auth } from '@/modules/auth'
import { routes as entity } from '@/modules/entity'
import { routes as views } from '@/views'

export default [...areas, ...auth, ...entity, ...views]
