import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '@/store'
// import { mapState } from 'vuex'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'active',
  routes
})

const publicRoutes = ['login', 'login/']

router.beforeEach((to, from, next) => {
  // const trem = mapState('auth', {
  //   trem: state => state.authenticated
  // })
  if (!store.state.auth.authenticated) {
    console.log('NAO autenticado: ')
    console.log('reposta depois da action', store.state.auth.authenticated)
    store.dispatch('auth/Authenticate')
    setTimeout(() => {}, 2000)

    if (!publicRoutes.find(element => to.name === element)) {
      return router.push({ name: 'login' })
    }
  }
  next()
})

export default router
