import { store as auth } from '@/modules/auth'
import { store as areas } from '@/modules/areas'
import { store as entity } from '@/modules/entity'
import { store as alert } from './modules/alert'
import { store as spinner } from './modules/spinner'

export default {
  auth,
  alert,
  areas,
  entity,
  spinner
}
