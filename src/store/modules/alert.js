export const store = {
  namespaced: true,
  state: {
    type: null,
    message: null,
    errors: []
  },
  actions: {
    success ({ commit }, message) {
      commit('success', message)
    },
    error ({ commit }, message) {
      commit('error', message)
    },
    clear ({ commit }) {
      commit('clear')
    }
  },
  mutations: {
    success (state, message) {
      state.type = 'alert-success'
      state.message = message
      state.error = []
    },
    error (state, message) {
      state.type = 'alert-danger'
      state.message = message.title
      state.errors = message.errors
    },
    clear (state) {
      state.type = null
      state.message = null
      state.erros = []
    }
  }
}
