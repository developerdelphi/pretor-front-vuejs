export const store = {
  namespaced: true,
  state: {
    loading: false
  },
  actions: {
    setLoading ({ commit }, payload) {
      commit('change', payload)
    }
  },
  mutations: {
    change (state, payload) {
      state.loading = payload
    }
  },
  getters: {
    hasLoading ({ loading }) {
      return loading
    }
  }
}
