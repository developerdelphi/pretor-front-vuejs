export const toast = {
  data: function () {
    return {
      info: {
        position: 'topCenter',
        iconColor: '#2c3e50',
        messageColor: '#2c3e50',
        messageSize: '18',
        overlay: false,
        class: 'alert alert-info alert-dismissible',
        theme: 'light',
        transitionIn: 'bounceInDown'
        // icon: 'icon fa fa-check'
      },
      login: {
        position: 'center',
        targetFirst: true,
        overlay: true,
        title: 'Login',
        titleSize: '20',
        titleLineHeight: '20',
        layout: 2,
        iconColor: '#2c3e50',
        messageColor: '#2c3e50',
        messageSize: '18',
        class: 'alert alert-info alert-dismissible',
        theme: 'light',
        transitionIn: 'fadeInRight'
        // icon: 'icon fa fa-check'
      },
      error: {
        position: 'topCenter',
        title: 'Atenção!',
        iconColor: 'rgb(145, 49, 39)',
        layout: 1,
        theme: 'light',
        messageColor: 'rgb(145, 49, 39)',
        messageSize: '18',
        transitionIn: 'bounceInDown',
        class: 'alert alert-danger alert-dismissible'
        // theme: 'dark',
        // icon: 'icon fa fa-ban'
      },
      success: {
        position: 'topCenter',
        title: 'Atenção!',
        iconColor: '#0f7864',
        layout: 1,
        theme: 'light',
        messageColor: '#0f7864',
        messageSize: '18',
        transitionIn: 'bounceInDown',
        class: 'alert alert-success alert-dismissible'
        // theme: 'dark',
        // icon: 'icon fa fa-ban'
      },
      deleteRecord: {
        position: 'center',
        iconColor: '#2c3e50',
        messageColor: '#2c3e50',
        messageSize: '18',
        class: 'alert alert-info alert-dismissible',
        theme: 'light',
        transitionIn: 'bounceInDown',
        timeout: 20000,
        close: false,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: '',
        message: '',
        layout: 2,
        buttons: [
          [
            '<button class="bg-green-500"><b>Sim</b></button>',
            function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button')
              // console.log('true')
              // return () => true
            },
            true
          ],
          [
            '<button>Não</button>',
            function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button')
            },
            false
          ]
        ],
        onClosing: function (instance, toast, closedBy) {
          console.info('Closing | closedBy: ' + closedBy)
        },
        onClosed: function (instance, toast, closedBy) {
          console.info('Closed | closedBy: ' + closedBy)
        }
      }
    }
  }
}
