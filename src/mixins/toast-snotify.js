export const toast = {
  data: function () {
    return {
      info: {
        timeout: 3000,
        showProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true
        // position: 'centerCenter',
        // backdrop: -1
      },
      login: {
        timeout: 3000,
        showProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true
        // position: 'centerCenter',
        // backdrop: 0
      },
      error: {
        timeout: 5000,
        showProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true
        // position: 'centerCenter',
        // backdrop: 0
      },
      success: {
        timeout: 3000,
        showProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true
        // position: 'centerCenter',
        // backdrop: 0
      }
    }
  }
}
