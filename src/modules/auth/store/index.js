import state from './state'
import mutations from './mutations'
import * as actions from './actions'
import getters from './getters'
// import store from './store'

export default {
  // store,
  actions,
  mutations,
  state,
  getters,
  namespaced: true
}
