import Vue from 'vue'
import * as types from './common-types'
import { Api } from '@/services/http/api'
import { toast as toastSnotify } from '@/mixins/toast-snotify'
import router from '@/router'

const setToastSnotify = toastSnotify.data()

export const Authenticate = async ({ dispatch }, payload) => {
  const token = localStorage.getItem('PretorToken')
  const user = localStorage.getItem('PretorUser')

  dispatch('setAuthToken', token)
  dispatch('setAuthUser', user)
  dispatch('setAuthenticated', true)
}

export const SignIn = async ({ dispatch }, payload) => {
  dispatch('spinner/setLoading', true, { root: true })

  localStorage.removeItem('PretorUser')
  localStorage.removeItem('PretorToken')

  return await Api.login(payload.email, payload.password).then(response => {
    if (response.status === 200) {
      const { user, token } = response.data
      dispatch('setAuthToken', token)
      dispatch('setAuthUser', user)
      dispatch('setAuthenticated', true)

      localStorage.setItem('PretorUser', JSON.stringify(user))
      localStorage.setItem('PretorToken', token)

      dispatch('spinner/setLoading', false, { root: true })

      Vue.$snotify.success(
        'Autorização concedida.',
        'Sucesso',
        setToastSnotify.success
      )

      router.push({ name: 'dashboard' })
      return true
    }

    if (response.status === 401) {
      Vue.$snotify.warning(
        'Usuário não autorizado, verifique suas credenciais!',
        'Atenção!',
        setToastSnotify.error
      )
    }

    if (response.status === 422) {
      Vue.$snotify.warning(
        'Usuário não autorizado, verifique suas credenciais!',
        'Atenção!',
        setToastSnotify.error
      )
    }

    dispatch('spinner/setLoading', false, { root: true })
    return response
  })
}

export const setAuthUser = ({ commit }, payload) => {
  commit(types.SET_USER, payload)
}

export const setAuthToken = ({ commit }, payload) => {
  commit(types.SET_TOKEN, payload)
}

export const setAuthenticated = ({ commit }, payload) => {
  commit(types.SET_AUTHENTICATED, payload)
}
