export default {
  authenticated ({ authenticated }) {
    return authenticated
  },
  username ({ user }) {
    return user.name
  },
  profile ({ user }) {
    return user
  },
  token ({ token }) {
    return token
  }
}
