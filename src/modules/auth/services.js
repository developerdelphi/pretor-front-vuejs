import Rest from '@/services/http/rest.class'
// import * as AuthAction from './store/actions'

/**
 * @typedef {AuthService}
 */
export default class AuthService extends Rest {
  /**
   * @type {String}
   */
  static resource = '/auth'

  async login (credentials) {
    return await this.post('/login', credentials)
  }

  async getUser () {
    return await this.post('/me')
  }

  setAuthenticated (auth) {
    localStorage.setItem('PretorAuth', JSON.stringify({ auth }))
  }
}
