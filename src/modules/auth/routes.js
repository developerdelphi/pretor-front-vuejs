export default [
  {
    path: '/login',
    component: () => import('@/core/components/layouts/LoginLayout'),
    children: [
      {
        path: '',
        name: 'login',
        component: () => import('./pages/Login')
      }
    ]
  }
]
