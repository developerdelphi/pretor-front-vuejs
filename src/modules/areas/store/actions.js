import Vue from 'vue'
import { Api } from '@/services/http/api'
import * as types from './common-types'
import router from '@/router'

import { toast as toastSnotify } from '@/mixins/toast-snotify'

const resource = '/areas'
const titleMsg = 'Área Processual'
const setToastSnotify = toastSnotify.data()
var action = ''

export const changeRecord = async ({ dispatch }, payload) => {
  // console.log('Chamando changeRecord:', payload)
  dispatch('setErrors', {})
  // Função que recebe os valores e constroi parametros para enviar para a API
  const search = fetchParams(payload)

  if (action === 'create') {
    dispatch('spinner/setLoading', true, { root: true })
    return await Api.create(search).then(response => responseApi(response))
  }

  if (action === 'update') {
    dispatch('spinner/setLoading', true, { root: true })
    return await Api.update(search).then(response => responseApi(response))
  }

  if (action === 'delete') {
    // this._vm.$snotify
    Vue.$snotify.error(
      'Deseja realmente remover do sistema a Área selecionada?',
      'Excluir Registro',
      {
        timeout: 5000,
        showProgressBar: true,
        closeOnClick: false,
        pauseOnHover: true,
        position: 'centerCenter',
        backdrop: 0.8,
        buttons: [
          {
            text: 'Sim',
            action: toast => {
              dispatch('spinner/setLoading', true, { root: true })
              return (
                Api.destroy(search).then(response => responseApi(response)),
                // (Vue.$snotify.backdrop = -1),
                // Vue.$snotify.clear(),
                Vue.$snotify.remove(toast.id)
              )
            },
            bold: false
          },
          {
            text: 'Não',
            action: toast => {
              // Vue.$snotify.clear()
              Vue.$snotify.remove(toast.id)
            }
          }
        ]
      }
    )
  }

  function responseApi (response) {
    console.log('ResponseAPI', response)
    // console.log('settings TOAST', settingsToast.data.success)

    /* Inicializa a variavel toast para receber parametros */
    // var setToast = toast.data()

    /* 200 = sucesso no update e delete */
    if (response.status === 200) {
      if (action === 'update') {
        dispatch('setRecord', response.data)
        Vue.$snotify.success(
          `${titleMsg} atualizada.`,
          'Sucesso',
          setToastSnotify.success
        )
      }
      if (action === 'delete') {
        dispatch('setRecord', {})
        router.push({ name: 'areas.index' })
        Vue.$snotify.success(
          `${titleMsg} removida.`,
          'Sucesso',
          setToastSnotify.success
        )
      }
      // izitoast.success(setToast.success)
      dispatch('setErrors', {})
    }
    /* 201 = sucesso no create */
    if (response.status === 201) {
      dispatch('setRecord', response.data)
      dispatch('setErrors', {})

      Vue.$snotify.success(
        `${titleMsg} criada.`,
        'Sucesso',
        setToastSnotify.success
      )
    }

    /* 400 = erro */
    if (response.status === 400) {
      Vue.$snotify.error(response.data.erro, 'Atenção', setToastSnotify.info)
    }

    /* 401 = Sem permissão de acesso, token expirou o tempo */
    if (response.status === 401) {
      Vue.$snotify.error(
        'Usuário sem permissão.',
        'Atenção',
        setToastSnotify.error
      )
      router.push({ name: 'login' })
    }

    /* 404 = erro, not found */
    if (response.status === 404) {
      Vue.$snotify.error(response.data.error, 'Atenção', setToastSnotify.error)
    }

    /* 422 = Validação de dados */
    if (response.status === 422) {
      const { errors } = response.data
      dispatch('setErrors', errors)

      Vue.$snotify.warning(
        'As informações não foram validadas.',
        'Atenção',
        setToastSnotify.info
      )
    }
    dispatch('spinner/setLoading', false, { root: true })
    return response
  }
}

export const changeParams = ({ state, dispatch }, payload) => {
  // atualiza o state atual com o payload do component
  const merge = Object.assign(state.params, payload.params)

  dispatch('setParams', merge)
  // atualiza o state.all com a pesquisa pelos parâmetros
  dispatch('getAll', { params: merge })
}

export const getAll = async ({ dispatch }, payload) => {
  var search = { resource: resource }
  if (payload) {
    search.payload = payload
  }
  // exibe o loading
  dispatch('spinner/setLoading', true, { root: true })

  return await Api.index(search).then(response => {
    if (response.status === 200) {
      dispatch('setGetAll', response.data)
      dispatch('setPaginate', response.paginate)
    }
    // Provavelmente este código deve ser removido
    if (response.status === 401) {
      router.push({ name: 'login' })
    }
    // fecha o loading
    dispatch('spinner/setLoading', false, { root: true })
  })
}

export const getRecord = async ({ dispatch }, payload) => {
  dispatch('spinner/setLoading', true, { root: true })
  var search = { resource: resource }
  if (payload) {
    search.payload = payload
  }
  console.log('chamou SHOW com payload', payload)

  return await Api.show(search).then(response => {
    if (response.status === 200) {
      console.log('Resposta na ACTION 200', response.data)
      dispatch('setRecord', response.data)
    }

    /* 400 = erro */
    if (response.status === 400) {
      Vue.$snotify.warning(response.data.error, 'Atenção', setToastSnotify.info)
    }

    if (response.status === 401) {
      dispatch('spinner/setLoading', false, { root: true })
      router.push('/login')
    }

    dispatch('spinner/setLoading', false, { root: true })

    return response
  })
}

export const setParams = ({ commit }, payload) => {
  commit(types.SET_PARAMS, payload)
}

export const setRecord = ({ commit }, payload) => {
  commit(types.SET_RECORD, payload)
}

export const setErrors = ({ commit }, payload) => {
  commit(types.SET_ERRORS, payload)
}

export const setGetAll = ({ commit }, payload) => {
  commit(types.SET_ALL, payload)
}

export const setPaginate = ({ commit }, payload) => {
  commit(types.SET_PAGINATE, payload)
}

function fetchParams (consult) {
  const params = {}
  params.payload = consult
  params.resource = resource
  action = consult.action
  return params
}
