export default [
  {
    path: '/areas',
    component: () =>
      import(
        /* webpackChunkName: "areas" */ '@/core/components/layouts/AppLayout'
      ),
    children: [
      {
        path: '/',
        name: 'areas.index',
        component: () =>
          import(
            /* webpackChunkName: "areas" */ '@/modules/areas/pages/AreasPage'
          )
      },

      {
        path: '/areas/new',
        name: 'areas.new',
        component: () =>
          import(
            /* webpackChunkName: "areas" */ '@/modules/areas/pages/AreasForm'
          )
      },

      {
        path: '/areas/edit/:id',
        name: 'areas.edit',
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "areas" */ '@/modules/areas/pages/AreasForm'
          )
      }
    ]
  }
]
