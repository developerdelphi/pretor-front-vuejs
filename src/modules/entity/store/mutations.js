import * as types from './common-types'
export default {
  [types.SET_ALL] (state, payload) {
    state.all = payload
  },

  [types.SET_PAGINATE] (state, payload) {
    state.pagination = payload
  },

  [types.SET_RECORD] (state, payload) {
    state.item = payload
  },

  [types.SET_ERRORS] (state, payload) {
    state.errors = payload
  },

  [types.SET_PARAMS] (state, payload) {
    state.params = payload
  }
}
