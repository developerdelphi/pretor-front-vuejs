export default {
  all: {},

  item: {},

  params: {
    per_page: 10,
    order: 'name',
    order_type: 'DESC'
  },

  pagination: {
    currentPage: 1,
    firstPage_url: null,
    from: 0,
    lastPage: 1,
    nextPage: null,
    path: null,
    perPage: 10,
    prevPageUrl: null,
    to: 0,
    total: 0
  },

  errors: {},

  fields: [
    {
      field: 'id',
      title: 'ID',
      visible: true,
      header: true,
      input: 'text',
      order: 1,
      filter: false,
      searched: false
    },
    {
      field: 'name',
      title: 'Entidade',
      visible: true,
      header: true,
      input: 'text',
      order: 2,
      filter: true,
      searched: true
    }
  ]
}
