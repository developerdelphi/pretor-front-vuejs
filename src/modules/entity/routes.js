export default [
  {
    path: '/entity',
    component: () =>
      import(
        /* webpackChunkName: "entity" */ '@/core/components/layouts/AppLayout'
      ),
    children: [
      {
        path: '/',
        name: 'entity.index',
        component: () =>
          import(
            /* webpackChunkName: "entity" */ '@/modules/entity/pages/EntityPage'
          )
      },

      {
        path: '/entity/new',
        name: 'entity.new',
        component: () =>
          import(
            /* webpackChunkName: "entity" */ '@/modules/entity/pages/EntityForm'
          )
      },

      {
        path: '/entity/edit/:id',
        name: 'entity.edit',
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "entity" */ '@/modules/entity/pages/EntityForm'
          )
      }
    ]
  }
]
