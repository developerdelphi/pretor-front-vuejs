export default [
  {
    path: '/',
    component: () =>
      import(
        /* webpackChunkName: "Home" */
        '@/core/components/layouts/PublicLayout'
      ),
    children: [
      {
        path: '',
        name: 'home',
        component: () =>
          import(/* webpackChunkName: "Home" */ '@/views/dashboard/Home')
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () =>
          import(/* webpackChunkName: "Home" */ '@/views/dashboard/Home')
      }
    ]
  }
]
