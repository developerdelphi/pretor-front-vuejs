import axios from 'axios'
import router from '../../router'

const BaseAxios = axios.create({
  baseURL: `${process.env.VUE_APP_BASE_API}/api`,
  headers: {
    'Content-Type': 'application/json; text/plain; charset=UTF-8'
  },
  transformResponse: [
    function (data) {
      return data
    }
  ]
})

BaseAxios.interceptors.request.use(
  config => {
    const tk = localStorage.getItem('PretorToken')
    if (tk) {
      config.headers.common.Authorization = `Bearer ${tk}`
    }
    return config
  },
  error => {
    console.log('erro do request baseaxios', error)
    return Promise.reject(error)
  }
)

BaseAxios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    const {
      config,
      response: { status }
    } = error
    if (status === 401 && !config.url.endsWith('auth/login')) {
      router.push('login')
    }
    return Promise.reject(error)
  }
)

export default BaseAxios
