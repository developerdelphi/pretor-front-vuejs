import BaseAxios from './conf'
// import router from '../../router'

export const Api = {
  login,
  logout,
  index,
  create,
  show,
  update,
  destroy
}

async function login (email, password) {
  const requestOptions = {
    url: 'auth/login',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({ email, password })
  }

  return await BaseAxios(requestOptions)
    .then(response => {
      console.log('Resposta da Api função login')
      return handleResponse(response)
    })
    .catch(erro => {
      console.log('Catch na Api função login', erro.response)
      return handleResponse(erro.response)
    })
  // .then(handleResponse)
  // .then(user => {
  //   // login successful if there's a jwt token in the response
  //   if (user.token) {
  //     // store user details and jwt token in local storage to keep user logged in between page refreshes
  //     localStorage.setItem('user', JSON.stringify(user))
  //   }

  //   return user
  // })
}

function logout () {
  // remove user from local storage to log user out
  localStorage.removeItem('PretorUser')
  localStorage.removeItem('PretorToken')
}

async function show (config) {
  console.log('Props SHOW na api***:', config)

  const url = config.payload
    ? `${config.resource}/${config.payload}`
    : `${config.resource}`

  // const data = config.payload

  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    url: url
  }

  return await BaseAxios(requestOptions)
    .then(response => {
      // console.log('Resposta da Api getAll')
      return handleResponse(response, true)
    })
    .catch(erro => {
      // console.log('Catch na Api getAll', erro.response)
      return handleResponse(erro.response)
    })
}

async function create (config) {
  console.log('Props da Config na api***:', config)
  // Montagem da url com módulo + action
  const url = config.action
    ? `${config.resource}/${config.action}`
    : `${config.resource}`
  const data = config.payload
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    url: url,
    data: data
  }

  return await BaseAxios(requestOptions)
    .then(response => {
      // console.log('Resposta da Api getAll')
      return handleResponse(response, true)
    })
    .catch(erro => {
      // console.log('Catch na Api getAll', erro.response)
      return handleResponse(erro.response)
    })
}

async function destroy (config) {
  console.log('Props na api DESTROY:', config)
  console.log('Props na api DESTROY PAYLOAD.id:', config.payload.id)
  // Montagem da url com módulo + action
  const params = paramsApi(config)

  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    url: params.url,
    data: params.data
  }

  return await BaseAxios(requestOptions)
    .then(response => {
      // console.log('Resposta da Api getAll')
      return handleResponse(response, true)
    })
    .catch(erro => {
      // console.log('Catch na Api getAll', erro.response)
      return handleResponse(erro.response)
    })
}

async function update (config) {
  console.log('Props na api UPDATE:', config)
  console.log('Props na api PAYLOAD.id:', config.payload.id)
  // Montagem da url com módulo + action
  const params = paramsApi(config)

  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    url: params.url,
    data: params.data
  }

  return await BaseAxios(requestOptions)
    .then(response => {
      // console.log('Resposta da Api getAll')
      return handleResponse(response, true)
    })
    .catch(erro => {
      // console.log('Catch na Api getAll', erro.response)
      return handleResponse(erro.response)
    })
}

function paramsApi (config) {
  const params = {}

  if (config.action) {
    params.url = `${config.resource}/${config.action}`
  } else if (config.payload.id) {
    params.url = `${config.resource}/${config.payload.id}`
  } else {
    params.url = `${config.resource}`
  }

  params.data = config.payload

  return params
}

async function index (config) {
  // console.log('Props da Config na api***:', config)
  // Montagem da url com módulo + action
  const url = config.action
    ? `${config.resource}/${config.action}`
    : `${config.resource}`
  // pega os parámetros da search
  const { params } = config.payload

  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    url: url,
    params: params
  }
  // console.log('Requisição via axios:', requestOptions)
  return await BaseAxios(requestOptions)
    .then(response => {
      // console.log('Resposta da Api getAll')
      return handleResponse(response, true)
    })
    .catch(erro => {
      // console.log('Catch na Api getAll', erro.response)
      return handleResponse(erro.response)
    })
}

function handleResponse (response) {
  var resp = {}
  if (response.status === 200) {
    const { data: dataStr } = response
    const { data: dataObj } = JSON.parse(dataStr)
    const {
      current_page: currentPage,
      first_page_url: firstPageUrl,
      from,
      last_page: lastPage,
      next_page: nextPage,
      path,
      per_page: perPage,
      prev_page_url: prevPageUrl,
      to,
      total
    } = JSON.parse(dataStr)

    var paginate = {}
    if (currentPage) {
      paginate = {
        currentPage: currentPage,
        firstPageUrl: firstPageUrl,
        from: from,
        lastPage: lastPage,
        nextPage: nextPage,
        path: path,
        perPage: perPage,
        prevPage_url: prevPageUrl,
        to: to,
        total: total
      }
    }

    if (paginate.currentPage) {
      resp = {
        status: 200,
        data: dataObj || null,
        paginate
      }
    } else {
      resp = {
        status: 200,
        data: dataObj || null
      }
    }

    return resp
  }

  if (response.status === 201) {
    return (resp = {
      status: 201,
      data: JSON.parse(response.data)
    })
  }

  if (response.status === 400) {
    return (resp = {
      status: 400,
      data: JSON.parse(response.data)
    })
  }

  if (response.status === 401) {
    return (resp = {
      status: 401,
      data: {
        message: 'Usuário não autorizado!'
      }
    })
  }

  if (response.status === 404) {
    return (resp = {
      status: 404,
      data: JSON.parse(response.data)
    })
  }

  if (response.status === 422) {
    resp = {
      status: 422,
      data: JSON.parse(response.data)
    }
    return resp
  }
}
