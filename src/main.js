import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Snotify, { SnotifyPosition } from 'vue-snotify'

/* Importação de estilos */
// import '@/styles/icons/fontAweSome'
import '@/assets/scss/app.css'
import { toast as toastSnotify } from './mixins/toast-snotify'
import 'vue-snotify/styles/material.css'
/* ----- */

import {
  faSpinner,
  faSearch,
  faPencilAlt,
  faTrashAlt,
  faListAlt,
  faUser,
  faBuilding,
  faUniversity,
  faUserAltSlash,
  faLandmark,
  faSquare,
  faUsers,
  faPlus,
  faKey,
  faEnvelope,
  faBalanceScaleRight,
  faInfoCircle,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAngleDown,
  faChevronDown,
  faCaretDown,
  faCaretUp,
  faAngleUp,
  faBackspace
} from '@fortawesome/free-solid-svg-icons'

/* Importação de componentes globais */
import HeaderPage from '@/core/components/HeaderPage'
import TableList from '@/core/components/table/TableList/Table'
import PreLoader from './core/components/preloader/PreLoader'

library.add(
  faUser,
  faUsers,
  faBuilding,
  faUniversity,
  faKey,
  faEnvelope,
  faAngleDown,
  faAngleUp,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faLandmark,
  faListAlt,
  faSquare,
  faPlus,
  faBalanceScaleRight,
  faAngleDown,
  faChevronDown,
  faChevronDown,
  faSpinner,
  faPencilAlt,
  faTrashAlt,
  faInfoCircle,
  faUserAltSlash,
  faCaretDown,
  faCaretUp,
  faSearch,
  faBackspace
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('TableList', TableList)
Vue.component('HeaderPage', HeaderPage)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('PreLoader', PreLoader)
/* --------------------------------- */

Vue.filter('truncate', function (value, limit) {
  if (value.length > limit) {
    value = value.substring(0, limit - 3) + '...'
  }

  return value
})

const confSnotifyToast = {
  toast: {
    position: SnotifyPosition.rightTop
  }
}

Vue.use(Snotify, confSnotifyToast)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  mixins: [toastSnotify],
  beforeCreate () {
    Vue.$snotify = this.$snotify
  },
  render: h => h(App)
}).$mount('#app')
