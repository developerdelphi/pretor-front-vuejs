import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faSearch,
  faPencilAlt,
  faTrashAlt,
  faListAlt,
  faUser,
  faBuilding,
  faUniversity,
  faLandmark,
  faSquare,
  faUsers,
  faPlus,
  faBalanceScale,
  faKey,
  faEnvelope,
  faBalanceScaleRight
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faUser,
  faUsers,
  faBuilding,
  faUniversity,
  faKey,
  faEnvelope,
  faLandmark,
  faListAlt,
  faSquare,
  faPlus,
  faBalanceScaleRight,
  faSearch,
  faPencilAlt,
  faTrashAlt,
  faBalanceScale
  // more icons go here
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
